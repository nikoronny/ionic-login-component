import { Component, Input, Output, EventEmitter } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from "@angular/forms";
import { AlertController } from "ionic-angular";

@Component({
    selector: "login-component",
    templateUrl: "login-component.html",
})
export class LoginComponent {
    @Input() title: string = 'Login';
    @Input() isPasswrodShow: boolean = false;
    @Input() buttonName: string = 'Login';
    @Output() componentValue: EventEmitter<string> = new EventEmitter<string>();

    private loginForm: FormGroup;
    passwordType: string = 'password';
    passwordIcon: string = 'eye-off';
    constructor(private formBuilder: FormBuilder, private alertCtrl: AlertController, ) {
        this.loginForm = this.formBuilder.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });
    }
    loginUser(loginData) {
        if (this.loginForm.valid) {
            this.componentValue.emit(loginData);
        }
        else {
            this.getFormError(this.loginForm);
        }
    }
    getFormError(form: FormGroup | FormArray | FormControl) {
        if (form instanceof FormGroup) {
            let errorcontrols: string[] = [];
            if (form.invalid) {
                for (let prop in form.controls) {
                    if (form.controls[prop] instanceof FormControl) {
                        if (form.controls[prop].invalid) {
                            if (form.controls[prop].errors['required']) {
                                let errors = prop + ' is Required !\n'
                                errorcontrols.push(errors);
                            } else if (form.controls[prop].errors['maxlength']) {
                                let errors = 'Maximum Length of ' + prop + ' is ' + form.controls[prop].errors['maxlength'].requiredLength + '\n';
                                errorcontrols.push(errors);
                            } else if (form.controls[prop].errors['minlength']) {
                                let errors = 'Minimum required length of ' + prop + ' is ' + form.controls[prop].errors['minlength'].requiredLength + '\n';
                                errorcontrols.push(errors);
                            } else if (form.controls[prop].errors['pattern']) {
                                let errors = prop + ' field pattern is not match\n';
                                errorcontrols.push(errors);
                            } else if (form.controls[prop].errors['max']) {
                                let errors = 'Maximum value of ' + prop + ' is ' + form.controls[prop].errors['max'].max + '\n';
                                errorcontrols.push(errors);
                            } else if (form.controls[prop].errors['min']) {
                                let errors = 'Minumum value of ' + prop + ' is ' + form.controls[prop].errors['min'].min + '\n';
                                errorcontrols.push(errors);
                            } else if (form.controls[prop].errors['email']) {
                                let errors = 'Invalid email address.' + '\n';
                                errorcontrols.push(errors);
                            }
                        }
                    } else if (form.controls[prop] instanceof FormArray) {
                        let counter = 1;
                        if (form.controls[prop].invalid) {
                            (<FormArray>form.controls[prop]).controls.forEach(control => {
                                if (!control.invalid)
                                    return;
                                if (control instanceof FormGroup) {
                                    for (let props in control.controls) {
                                        if (control.controls[props] instanceof FormControl) {
                                            if (control.controls[props].invalid) {
                                                if (control.controls[props].errors['required']) {
                                                    let errors = props + ' ' + counter + ' is required field\n';
                                                    errorcontrols.push(errors);
                                                } else if (control.controls[props].errors['maxlength']) {
                                                    let errors = 'Maximum Length of ' + prop + ' ' + counter + ' ' + props + ' is ' + control.controls[props].errors['maxlength'].requiredLength + '\n';
                                                    errorcontrols.push(errors);
                                                } else if (control.controls[props].errors['minlength']) {
                                                    let errors = 'Minumum Length of ' + prop + ' ' + counter + ' ' + props + ' is ' + control.controls[props].errors['minlength'].requiredLength + '\n';
                                                    errorcontrols.push(errors);
                                                } else if (control.controls[props].errors['pattern']) {
                                                    let errors = props + ' ' + counter + ' field pattern is not match\n';
                                                    errorcontrols.push(errors);
                                                } else if (control.controls[props].errors['min']) {
                                                    let errors = 'Minimum Value of ' + prop + ' ' + counter + ' ' + props + ' is ' + control.controls[props].errors['min'].min + '\n';
                                                    errorcontrols.push(errors);
                                                } else if (control.controls[props].errors['max']) {
                                                    let errors = 'Maximum Value of ' + prop + ' ' + counter + ' ' + props + ' is ' + control.controls[props].errors['max'].max + '\n';
                                                    errorcontrols.push(errors);
                                                } else if (control.controls[props].errors['email']) {
                                                    let errors = 'Invalid email address of ' + prop + ' ' + counter + '\n';
                                                    errorcontrols.push(errors);
                                                }
                                            }
                                        } else if (control.controls[props] instanceof FormArray) {
                                            if (control.controls[props].invalid) {
                                                this.getFormError(control);
                                            }
                                        }
                                    }
                                }
                                counter++;
                            });

                        }
                    }
                }
            }
            this.showFormAlertArray(errorcontrols);
        }
    }
    showFormAlertArray(error) {
        let alert = this.alertCtrl.create({
            title: 'Invalid!',
            buttons: ['OK'],
            subTitle: error.join('')
        });
        alert.present();
    }
    show = () => {
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }
}